const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const graphqlHTTP = require('express-graphql')
const graphqlSchema = require('./gql-schema')


/* DATABASE CONNECTION */
mongoose.connect('mongodb://localhost:27017/capstone3', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
mongoose.connection.once('open', ()=>{
	console.log("Now connected to Local MongoDB Server");
})

/* MIDDLEWARE */
app.use(cors())
//app.use('/graphql', graphqlHTTP({ schema: graphqlSchema, graphiql: true}))

/* SERVER INITIALIZATION */
app.listen(4000, () => {
	console.log("Now listening for requests on port 4000")
	// console.log(Task)
})