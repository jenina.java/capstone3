const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema= new Schema({
	productName:{
		type: String,
		required: true
	},
	productPrice:{
		type: String,
		required:true
	}
	productDescription:{
		type: String,
		required:true
	},
	productImagePath:{
		type: String,
		required:true
	}
},
	{
		timeStamps:true
	}
)
module.exports = mongoose.model('Product', productSchema)