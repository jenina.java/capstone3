const mongoose = require('mongoose')
const Schema = mongoose.Schema

const transactionSchema= new Schema({
	transactionCode:{
		type: String,
		required: true
	},
	productId:{
		type: String,
		required:true
	}
},
	{
		timeStamps:true
	}
)
module.exports = mongoose.model('Transaction', transactionSchema)