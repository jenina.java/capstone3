const mongoose = require('mongoose')
const Schema = mongoose.Schema

/*
	name, email, username password, role, mobileNo
*/

const userSchema= new Schema({
	firstName:{
		type: String,
		required: true
	},
	lastName:{
		type: String,
		required: true
	},
	address:{
		type: String,
		required:true
	}
	email:{
		type: String,
		required:true
	},
	username:{
		type: String,
		required:true
	},
	password:{
		type: String,
		required:true
	},
	role:{
		type: String,
		required:true
	},
	mobileNo:{
		type: String,
		required:true
	},
},
	{
		timeStamps:true
	}
)
module.exports = mongoose.model('User', userSchema)